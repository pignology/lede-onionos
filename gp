#!/bin/sh

DATE=`date +%Y%m%d-%H%M`

git add . --all
git commit -m $DATE
git push -u origin master
